import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppLogger } from './common/appLogger';
import { WitnessModule } from './witness/witness.module';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(WitnessModule, {
    logger: new AppLogger(),
  });

  const config = new DocumentBuilder()
    .setTitle('Witness API')
    .setDescription('Witness API for reporting')
    .setVersion('0.0.1')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);

  logger.log(`Application is running on: ${await app.getUrl()}`);
  logger.log(`Swagger is running on: ${await app.getUrl()}/api`);
}
bootstrap();
