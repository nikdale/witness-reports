import { Body, Controller, Param, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { WitnessReportRequestBody } from './witness.types';

@ApiTags('Witness')
@Controller('witness')
export class WitnessController {
  @ApiOperation({
    summary: 'Report name',
    description: 'Report a name from DB.',
  })
  // @ApiOkResponse({
  //   type:,
  // })
  @Post('/report')
  async getCompetitionById(
    @Body() request: WitnessReportRequestBody,
  ): Promise<any> {
    console.log(request);
    return true;
  }
}
