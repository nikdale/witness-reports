import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsDefined, IsPhoneNumber } from 'class-validator';

export class WitnessReportRequestBody {
  @ApiProperty({ required: true })
  @IsString()
  @MinLength(1)
  @IsDefined()
  name: string;

  @ApiProperty({ required: true, example: '+38139555333' })
  @IsPhoneNumber()
  @IsDefined()
  phoneNumber: string;
}
