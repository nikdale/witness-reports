import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
// import { createGenericResponse } from '../../http/response';

@Catch(Error)
export class GenericErrorFilter implements ExceptionFilter<Error> {
  catch(error: Error, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    console.log(response);
    console.log(error);
    // response.status(500).json(
    //   createGenericResponse(
    //     null,
    //     [
    //       {
    //         message: error.message,
    //         code: 500,
    //         name: "SYSTEM_ERROR",
    //         payload: null,
    //       },
    //     ],
    //     500,
    //   ),
    // );
  }
}
