import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
// import { createGenericResponse } from '../../http/response';

// Helper interface
interface HttpError {
  statusCode: number;
  message: string;
  error: string;
}

@Catch(HttpException)
export class HttpExceptionErrorFilter
  implements ExceptionFilter<HttpException>
{
  catch(httpException: HttpException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const { statusCode = 500, message } =
      httpException.getResponse() as HttpError;
    response
      .status(statusCode)
      .json
      // createGenericResponse(
      //   null,
      //   [
      //     {
      //       message: message,
      //       code: statusCode,
      //       name: 'HTTP_EXCEPTION',
      //       payload: null,
      //     },
      //   ],
      //   statusCode,
      // ),
      ();
  }
}
