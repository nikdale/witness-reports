export const AppLogger = jest.fn().mockReturnValue({
  log: jest.fn().mockReturnValue(null),
  error: jest.fn().mockReturnValue(null),
  warn: jest.fn().mockReturnValue(null),
  debug: jest.fn().mockReturnValue(null),
  verbose: jest.fn().mockReturnValue(null),
});
